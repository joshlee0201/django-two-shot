from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return redirect("home")


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/category_create.html"
    fields = ["name"]

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("categories_list")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/category_list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/account_create.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        account.save()
        return redirect("account_list")


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/account_list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)
